SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

CREATE TABLE IF NOT EXISTS db_order (
id int(10) unsigned NOT NULL,
  `user` int(11) NOT NULL,
  user_edit int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  phone varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  address text COLLATE utf8_unicode_ci NOT NULL,
  years year(4) NOT NULL DEFAULT '0000',
  months varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000-00',
  months_one varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '00',
  days date NOT NULL DEFAULT '0000-00-00',
  days_one varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '00',
  times time NOT NULL DEFAULT '00:00:00',
  created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO db_order (id, user, user_edit, status, number, name, phone, address, years, months, months_one, days, days_one, times, created_at, updated_at) VALUES
(6, 1, 0, 1, '', 'Osman', '(324) 324 32 44', 'Batman Merkez', 2015, '2015-06', '07', '2015-07-11', '11', '00:00:00', '2015-07-12 13:25:40', '2015-07-12 13:25:40'),
(7, 1, 0, 1, '', 'ahmet', '(232) 234 32 42', 'adres', 2015, '2015-07', '07', '2015-07-11', '12', '00:00:00', '2015-07-12 13:29:58', '2015-07-12 13:29:58'),
(8, 1, 0, 1, '', 'CİHAT TATAR', '(324) 234 32 42', 'Gülyaka Karabağlar/izmir', 2015, '2015-07', '07', '2015-07-12', '12', '13:31:59', '2015-07-12 13:31:59', '2015-07-12 14:01:32'),
(9, 1, 0, 1, '', 'Cihat Tatar', '(223) 432 43 24', '', 2015, '2015-07', '07', '2015-07-12', '12', '13:42:48', '2015-07-12 13:42:48', '2015-07-12 13:42:48');

CREATE TABLE IF NOT EXISTS db_order_products (
id int(10) unsigned NOT NULL,
  product_id int(11) NOT NULL,
  order_id int(11) NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO db_order_products (id, product_id, order_id, number) VALUES
(40, 1, 6, 3),
(41, 2, 6, 3),
(42, 3, 6, 4),
(43, 4, 6, 2),
(44, 5, 6, 1),
(45, 6, 6, 0),
(46, 7, 6, 0),
(47, 8, 6, 0),
(48, 9, 6, 0),
(49, 10, 6, 0),
(50, 11, 6, 0),
(51, 12, 6, 0),
(52, 13, 6, 0),
(53, 1, 7, 3),
(54, 2, 7, 1),
(55, 3, 7, 0),
(56, 4, 7, 0),
(57, 5, 7, 0),
(58, 6, 7, 0),
(59, 7, 7, 0),
(60, 8, 7, 0),
(61, 9, 7, 0),
(62, 10, 7, 0),
(63, 11, 7, 0),
(64, 12, 7, 0),
(65, 13, 7, 0),
(66, 1, 8, 1),
(67, 2, 8, 3),
(68, 3, 8, 4),
(69, 4, 8, 1),
(70, 5, 8, 2),
(71, 6, 8, 3),
(72, 7, 8, 0),
(73, 8, 8, 0),
(74, 9, 8, 2),
(75, 10, 8, 0),
(76, 11, 8, 0),
(77, 12, 8, 0),
(78, 13, 8, 0),
(79, 1, 9, 2),
(80, 2, 9, 3),
(81, 3, 9, 0),
(82, 4, 9, 0),
(83, 5, 9, 1),
(84, 6, 9, 0),
(85, 7, 9, 0),
(86, 8, 9, 3),
(87, 9, 9, 0),
(88, 10, 9, 0),
(89, 11, 9, 0),
(90, 12, 9, 0),
(91, 13, 9, 0);

CREATE TABLE IF NOT EXISTS db_products (
id int(10) unsigned NOT NULL,
  `user` int(11) NOT NULL,
  user_edit int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  title varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  price decimal(5,2) NOT NULL,
  description text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO db_products (id, user, user_edit, type, title, price, description, status, created_at, updated_at) VALUES
(1, 1, 1, 1, 'Adana', '5.00', 'Adana', 1, '0000-00-00 00:00:00', '2015-07-10 23:11:43'),
(2, 1, 0, 1, 'Urfa', '5.00', '', 1, '2015-07-10 22:59:07', '2015-07-10 22:59:07'),
(3, 1, 1, 1, 'Tavuk şiş', '3.50', '', 1, '2015-07-10 22:59:33', '2015-07-12 01:17:35'),
(4, 1, 0, 1, 'Et şiş', '6.00', '', 1, '2015-07-10 23:12:19', '2015-07-10 23:12:19'),
(5, 1, 0, 1, 'Ciğer şiş', '4.00', '', 1, '2015-07-10 23:12:33', '2015-07-10 23:12:33'),
(6, 1, 0, 1, 'Kanat', '8.50', '', 1, '2015-07-11 13:13:37', '2015-07-11 13:13:37'),
(7, 1, 0, 2, 'Ayran', '1.00', '', 1, '2015-07-11 13:19:26', '2015-07-11 13:19:26'),
(8, 1, 0, 2, 'Kola', '1.00', '', 1, '2015-07-11 13:19:41', '2015-07-11 13:19:41'),
(9, 1, 1, 2, 'Fanta', '1.00', '', 1, '2015-07-11 13:19:49', '2015-07-11 13:20:43'),
(10, 1, 0, 2, 'Gazoz', '1.00', '', 1, '2015-07-11 13:20:57', '2015-07-11 13:20:57'),
(11, 1, 0, 2, 'Şalgam', '1.50', '', 1, '2015-07-11 13:21:32', '2015-07-11 13:21:32'),
(12, 1, 0, 2, 'Su', '0.75', '', 1, '2015-07-11 13:21:48', '2015-07-11 13:21:48'),
(13, 1, 0, 2, 'Meyve suyu', '1.00', '', 1, '2015-07-11 13:22:15', '2015-07-11 13:22:15');

CREATE TABLE IF NOT EXISTS db_users (
id int(10) unsigned NOT NULL,
  username varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  recovery varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  remember_token varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO db_users (id, username, password, email, name, recovery, status, remember_token, created_at, updated_at) VALUES
(1, 'osman', '$2y$10$Gute//o5XZKU8vasQDxFqeudL7sL.eHskhgH5OQK.dryWI/CplWM6', 'osmnylmz@outlook.com', 'Osman YILMAZ', 'yilmaz', 1, 'rWoBDqVfAZAQIVpzo2q18mJdqspbW88QzRaNJomRK591jw6J1n6Sw4rhTEcG', '0000-00-00 00:00:00', '2015-07-13 18:59:36'),
(2, 'admin', '$2y$10$Gute//o5XZKU8vasQDxFqeudL7sL.eHskhgH5OQK.dryWI/CplWM6', 'admin@admin.com', 'Admin', 'admin', 1, '', '0000-00-00 00:00:00', '2015-07-13 19:00:24');

CREATE TABLE IF NOT EXISTS migrations (
  migration varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  batch int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO migrations (migration, batch) VALUES
('2015_07_10_194624_create_users_table', 1),
('2015_07_10_195258_create_table_products', 2),
('2015_07_10_195713_create_table_order', 3),
('2015_07_10_201610_create_table_products_order', 4);


ALTER TABLE db_order
 ADD PRIMARY KEY (id);

ALTER TABLE db_order_products
 ADD PRIMARY KEY (id);

ALTER TABLE db_products
 ADD PRIMARY KEY (id);

ALTER TABLE db_users
 ADD PRIMARY KEY (id);


ALTER TABLE db_order
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE db_order_products
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
ALTER TABLE db_products
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
ALTER TABLE db_users
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
