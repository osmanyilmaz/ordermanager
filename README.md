# Elixir Sipariş Yönetimi
Sade Lokanta/Kebapçı Otomasyonu   
Migrations ile kurulumunuzu yapabilirsiniz. 
Örnek veritabanı dosyasını SQL.sql olarak proje ana dizininde mevcuttur.
# Laravel 
Laravel 4.2 Sürümü kullanılarak yapılmıştır.

# Kurulum
### Uzun Kurulum:
Konsoldan laravel 4.2 sürümününun kurulumunu yapıyoruz
```composer create-project laravel/laravel elixir-ordermanager "4.2.*"```
Ardından dosyayı indiriyoruz; 
**https://github.com/osmanyilmazco/ordermanager/archive/master.zip**
Laravel 4.2 kurulumunu yapmış olduğumuz klasörün içine indirdimiz dosyalarımızı yapıştırıyoruz.
En son olarak composer.json içindede belirtilen eklentilerin kurulumununda yapılması için konsoldan projenin klasörüne girip ```composer update``` Komutunu yazıyoruz
### Kısa Kurulum:
**https://github.com/osmanyilmazco/ordermanager/archive/master.zip** 
Dosyayı indirip localhost'a atıyoruz ve konsoldan ```composer install``` Komutunu yazdırıyoruz

## Veritabanı Ayarları
kök dizindeki ```app/config/databaseconnect.php``` dosyasının içine veritabanı bilgilerimizi yazıyoruz
**Örnek bağlantı**
```php
return array(
	'host'      => 'localhost',
	'database'  => 'laravel4_birtat',
	'username'  => 'root',
	'password'  => '',
); 
```


### En son olarak migrasyonların çalışması için
```
php artisan migrate
```
komutumuzu konsola yazdıktan sonra sistemi kullanmaya başlayabiliriz. :)


# Önizleme
![Elixir](http://indir.astald.com/dosyalar/1_db_56e4117db2e41.png)
![Elixir](http://indir.astald.com/dosyalar/2_db_56e4117db8293.png)
![Elixir](http://indir.astald.com/dosyalar/3_db_56e4117dbc0a9.png)
![Elixir](http://indir.astald.com/dosyalar/7_db_56e4117dd4a2a.png)
![Elixir](http://indir.astald.com/dosyalar/8_db_56e4117dd7a44.png)
![Elixir](http://indir.astald.com/dosyalar/4_db_56e4117dc018e.png)
![Elixir](http://indir.astald.com/dosyalar/5_db_56e4117dd0989.png)
![Elixir](http://indir.astald.com/dosyalar/9_db_56e4117ddf882.png)

# Bilgi
Anlayışınız için teşekkürler.

www.astald.com
