<?php

/*
 * Elixir Otomasyon
 * Osman YILMAZ
 * www.astald.com
 * https://github.com/astald/elixir-ordermanager
 */

class AdminController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Astald Admin Home Controller
	|--------------------------------------------------------------------------
	*/ 
	
	/* Güvenlik kontrol */
	public function __construct() 
	{
		$this->beforeFilter('csrf',array('on'=>array('post','delete','put')));
		$this->beforeFilter('ajax',array('on'=>array('delete','put')));
	}

}
