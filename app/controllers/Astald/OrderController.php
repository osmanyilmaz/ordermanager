<?php

/*
 * Elixir Otomasyon
 * Osman YILMAZ
 * www.astald.com
 * https://github.com/astald/elixir-ordermanager
 */

class Astald_OrderController extends AdminController {
	
	public function getIndex()
	{ 
		$order_years = Order::where('years',date('Y'))->get();
		$order_months = Order::where('months',date('Y-m'))->get();
		$order_days = Order::where('days',date('Y-m-d'))->get();
		$order_all = Order::all();
		$list = Order::orderBy('id','desc')->where('status',1)->paginate('12'); 
		return View::make('order.list', compact('list','order_years','order_months','order_days','order_all'))->with(['title'=>'Siparişler','count_static'=>true,'date_visible'=>true,'price_all'=>0,'datetime'=>'all','datename'=>'Tüm ']);
	}
	public function getList($date)
	{ 
		$limit = 12;
		if($date=='today') 
		{
			$datename = "Bu <u>gün</u>e ait olan";
			$list = Order::orderBy('id','desc')->where('status',1)->where('days',date('Y-m-d'))->paginate($limit);
		}
		else if($date=='month') 
		{
			$datename = "Bu <u>ay</u>a ait olan";
			$list = Order::orderBy('id','desc')->where('status',1)->where('months',date('Y-m'))->paginate($limit);
		}
		else if($date=='year') 
		{
			$datename = "Bu <u>yıl</u>a ait olan";
			$list = Order::orderBy('id','desc')->where('status',1)->where('years',date('Y'))->paginate($limit);
		}
		else 
		{
			$datename = "Tüm";
			$list = Order::orderBy('id','desc')->where('status',1)->paginate($limit);
		}
		$order_years = Order::where('years',date('Y'))->get();
		$order_months = Order::where('months',date('Y-m'))->get();
		$order_days = Order::where('days',date('Y-m-d'))->get();
		$order_all = Order::all();
		return View::make('order.list', compact('list','order_years','order_months','order_days','order_all'))->with(['title'=>'Siparişler','count_static'=>true,'date_visible'=>true,'price_all'=>0,'datetime'=>$date,'datename'=>$datename]);
	}
	public function getSearch()
	{
		$search = e(Input::get('s'));
		$list = Order::where('name','like',"%$search%")->orwhere('phone','like',"%$search%")->orwhere('address','like',"%$search%")->paginate(12)->appends(array('s'=>$search));
		return View::make('order.list', compact('list','order_years','order_months','order_days','order_all'))->with(['title'=>'Siparişler','count_static'=>false,'date_visible'=>true,'price_all'=>0,'datetime'=>'all','datename'=>"<u>{$search}</u> için "]);
	}
	public function getAdd()
	{
		$list_product = Product::where('status',1)->get();
		return View::make('order.add',compact('list_product'))->with(['title'=>'Sipariş Ekle','selectnumber'=>0]);
	}
	public function postAdd()
	{
		if(Input::get('item_val')==null or Input::get('item_val')=='')
			return Redirect::back()->with(['message'=>'true', 'title'=>'Hata!', 'text'=>'Tanımsız hata ile karşılaşıldı.', 'type'=>'error']);
		$order = new Order;
		$order->status = 1;
		$order->user = Auth::user()->id; 
		$order->name = Input::get('name'); 
		$order->phone = Input::get('phone'); 
		$order->address = Input::get('address'); 
		$order->years = date('Y');
		$order->months = date('Y-m');
		$order->months_one = date('m');
		$order->days = date('Y-m-d');
		$order->days_one = date('d');
		$order->times = date('H:i:s');
		$order->save();
		if($order->save())
		{ 
			$items = Input::get('item_val');
			foreach ($items as $key=>$item) 
			{ 
				if(!OrderToProduct::where('order_id',$order->id)->where('product_id',$key)->get())
					$result = false;
				else 
				{
					$ord = new OrderToProduct;
					$ord->order_id = $order->id;
					$ord->product_id = $key;
					$ord->number = $item;
					$ord->save();
					if($ord->save())
						$result = true;
					else 
						$result = false;
				}
			}
			if($result)
				return Redirect::to('order')->with(['message'=>'true', 'title'=>'Tebrikler!', 'text'=>'Sipariş başarıyla oluşturuldu.', 'type'=>'success']);
			else 
				return Redirect::back()->with(['message'=>'true', 'title'=>'Hata!', 'text'=>'Sipariş oluşturulamadı.', 'type'=>'error']);
		}
		else 
			return Redirect::back()->with(['message'=>'true', 'title'=>'Hata!', 'text'=>'Sipariş oluşturulamadı.', 'type'=>'error']);
	}
	public function getEdit($id)
	{ 
		$edit = Order::find($id);
		$list_product = OrderToProduct::where('order_id',$edit->id)->get();
		if(!$edit) return Redirect::to('product');
		return View::make('order.edit',compact('edit','list_product'))->with('title','Sipariş Düzelt');
	}
	public function postEdit($id)
	{
		$edit = Order::find($id);
		if(!$edit) return Redirect::to('order');
		$edit->name = Input::get('name');
		$edit->phone = Input::get('phone');
		$edit->address = Input::get('address');
		$edit->save();
		if($edit->save())
			return Redirect::back()->with(['message'=>'true', 'title'=>'Tebrikler!', 'text'=>'Sipariş kaydı başarıyla güncelleştirildi.', 'type'=>'success']);
		else 
			return Redirect::back()->with(['message'=>'true', 'title'=>'Hata!', 'text'=>'Sipariş güncelleştirilemedi.', 'type'=>'error']);
	}
	public function postEdittable()
	{
		if(Input::get('orderid')==null or Input::get('productid')==null)
			return 0;
		$edit = OrderToProduct::where('product_id',Input::get('productid'))->where('order_id',Input::get('orderid'))->first();
		$edit->number = Input::get('number');
		$edit->save();
		if($edit->save())
			return 1;
		else 
			return 0;
	}
	public function getDelete($id)
	{
		$order = Order::find($id);
		if(!$order) return Redirect::to('order');
		$lists = OrderToProduct::where('order_id',$order->id)->get();
		if(count($lists)>0)
		{
			foreach ($lists as $item) {
				$item->delete();
			}
		}
		$order->delete();
		if(!$order->delete())
			return Redirect::to('order')->with(['message'=>'true', 'title'=>'Tebrikler!', 'text'=>'Sipariş kaydı başarıyla silindi.', 'type'=>'success']);
		else 
			return Redirect::back()->with(['message'=>'true', 'title'=>'Hata!', 'text'=>'Sipariş kaydı silinemedi! Lütfen daha sonra tekrar deneyiniz.', 'type'=>'error']);
	}
}
