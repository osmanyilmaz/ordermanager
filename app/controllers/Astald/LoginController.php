<?php

/*
 * Elixir Otomasyon
 * Osman YILMAZ
 * www.astald.com
 * https://github.com/astald/elixir-ordermanager
 */

class Astald_LoginController extends HomeController {

	public function login()
	{
		// return Hash::make('osman');
		if(Auth::check())  
			return Redirect::to('/'); 
		return View::make('login.login');
	} 
	public function logout()
	{
		if(Auth::check()) Auth::logout();
		return Redirect::to('login');
	}
	public function logincontroll()
	{ 
		$validation = Validator::make(Input::all(), User::$user_validate);
		$validation_message = $validation->messages();
		if($validation->fails())
			return Redirect::back()->with(['messageControl'=>'true', 'title'=>'Uyarı!', 'text'=>'Kullanıcı adı veya şifre boş', 'type'=>'warning']);
		$userLogin = ['username'=>e(Input::get('username')),'password'=>e(Input::get('password'))];
		$login = Auth::attempt($userLogin, Input::get('remember'));
		if($login) 
			return Redirect::intended('home')->with(['messageControl'=>'true','title'=>'Hoşgeldiniz','text'=>'Sayın <b>'.Auth::user()->name.'</b> sisteme bağlandınız','type'=>'success']);
		else 
			return Redirect::back()->with(['messageControl'=>'true', 'title'=>'Giriş Hatalı!', 'text'=>'Kullanıcı adı veya şifre yanlış', 'type'=>'danger']);
	}
	public function password()
	{

		if(Auth::check())  
			return Redirect::to('/'); 
		return View::make('login.password');
	}
	public function passwordcontroll()
	{
		$validation = Validator::make(Input::all(), User::$password_validate);
		if($validation->fails())
			return Redirect::back()->with(['messageControl'=>'true', 'title'=>'Uyarı!', 'text'=>'Kullanıcı adı veya kurtarma alanı boş', 'type'=>'warning']);
		$pass = User::where('username',Input::get('username'))->where('recovery',Input::get('recovery'))->first();
		if(count($pass)>0)
			return Redirect::to('recovery/'.Astald::Password($pass->username).'/'.Astald::Password($pass->recovery).'/i'.$pass->id);
		else  
			return Redirect::back()->with(['messageControl'=>'true', 'title'=>'Hata!', 'text'=>'Girmiş olduğunuz bilgileri kontrol ediniz.', 'type'=>'danger']);
	}
	public function recovery($name,$recovery,$id)
	{
		$user = User::findOrFail(e($id));
		if(count($user)>0)
		{
			if((Astald::Password($user->username) == e($name)) and (Astald::Password($user->recovery) == e($recovery)))
				return View::make('login.recovery')->with('i',e($id));
			else 
				return Redirect::to('password');	
		}
		else 
			return Redirect::to('password');			
	}
	public function recoverycontroll()
	{
		$validation = Validator::make(Input::all(), User::$recovery_validate);
		if($validation->fails())
			return Redirect::back()->with(['messageControl'=>'true', 'title'=>'Uyarı!', 'text'=>'Yeni şifrenizi girmediniz.', 'type'=>'warning']);
		$pass = User::findOrFail(e(Input::get('s')));
		if(count($pass)>0)
		{
			$pass->password = Hash::make(Input::get('newpassword'));
			$pass->save();
			if($pass->save())
				return Redirect::to('login')->with(['messageControl'=>'true', 'title'=>'Tebrikler!', 'text'=>'Yeni şifrenizle giriş yapabilirsiniz.', 'type'=>'success']);
			else 
				return Redirect::back();
		}
		else 
			return Redirect::back();			
	}
}
