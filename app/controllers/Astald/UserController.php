<?php

/*
 * Elixir Otomasyon
 * Osman YILMAZ
 * www.astald.com
 * https://github.com/astald/elixir-ordermanager
 */

class Astald_UserController extends AdminController {
	
	public function getIndex()
	{
		$list = Order::where('status',1)->get(); // ->paginate();
		return View::make('order.list', compact('list'))->with('title','Siparişler');

	}
	public function getAdd()
	{
		$list_product = Product::where('status',1)->get();
		return View::make('order.add',compact('list_product'))->with('title','Sipariş Ekle');
	}
	public function postAdd()
	{
		return Input::all();
	}

}
