<?php

/*
 * Elixir Otomasyon
 * Osman YILMAZ
 * www.astald.com
 * https://github.com/astald/elixir-ordermanager
 */

class Astald_ProductsController extends AdminController {

	public function getIndex()
	{
		$list = Product::where('status',1)->get();
		return View::make('product.list', compact('list'))->with('title','Ürünler');
	}
	public function getAdd()
	{
		return View::make('product.add')->with('title','Ürün Ekle');
	}
	public function postAdd()
	{
		$validation = Validator::make(Input::all(), Product::$validation);
		$validation_message = $validation->messages();
		if($validation->fails())
			return Redirect::back()->with(['message'=>'true', 'title'=>'Uyarı!', 'text'=>'Ürün başlığı veya ürün fiyat alanını boş bıraktınız. Lütfen tüm alanların doldurunuz.', 'type'=>'warning']);
		$add = New Product;
		$add->user = Auth::user()->id;
		$add->type = Input::get('type');
		$add->title = Input::get('title');
		$add->price = Input::get('price');
		$add->save();
		if($add->save())		
			return Redirect::to('product')->with(['message'=>'true', 'title'=>'Tebrikler!', 'text'=>'Ürün kaydı başarıyla oluşturuldu.', 'type'=>'success']);
		else 
			return Redirect::back()->with(['message'=>'true', 'title'=>'Hata!', 'text'=>'Ürün kaydı oluşturulamadı! Lütfen daha sonra tekrar deneyiniz.', 'type'=>'error']);
	}
	public function getEdit($id)
	{
		$edit = Product::find($id);
		if(!$edit) return Redirect::to('product');
		return View::make('product.edit',compact('edit'))->with('title','Ürün Düzelt');
	}
	public function postEdit($id)
	{
		$edit = Product::find($id);
		if(!$edit) return Redirect::to('product');
		$validation = Validator::make(Input::all(), Product::$validation);
		if($validation->fails())
			return Redirect::back()->with(['message'=>'true', 'title'=>'Uyarı!', 'text'=>'Ürün başlığı veya ürün fiyat alanını boş bıraktınız. Lütfen tüm alanların doldurunuz.', 'type'=>'warning']);

		$edit->user_edit = Auth::user()->id;
		$edit->type = Input::get('type');
		$edit->title = Input::get('title');
		$edit->price = Input::get('price');
		$edit->save();
		if($edit->save())
			return Redirect::to('product')->with(['message'=>'true', 'title'=>'Tebrikler!', 'text'=>'Ürün kaydı başarıyla düzeltildi.', 'type'=>'success']);
		else 
			return Redirect::back()->with(['message'=>'true', 'title'=>'Hata!', 'text'=>'Ürün kaydı düzeltilemedi! Lütfen daha sonra tekrar deneyiniz.', 'type'=>'error']);
	}
	public function getDelete($id)
	{
		$delete = Product::findOrFail($id);
		if(!$delete) return Redirect::to('product');
		$delete->delete();
		if(!$delete->delete())
			return Redirect::to('product')->with(['message'=>'true', 'title'=>'Tebrikler!', 'text'=>'Ürün kaydı başarıyla silindi.', 'type'=>'success']);
		else 
			return Redirect::to('product')->with(['message'=>'true', 'title'=>'Hata!', 'text'=>'Ürün kaydı silinemedi! Lütfen daha sonra tekrar deneyiniz.', 'type'=>'error']);
	}
}
