<?php

/*
 * Elixir Otomasyon
 * Osman YILMAZ
 * www.astald.com
 * https://github.com/astald/elixir-ordermanager
 */

class Astald_HomeController extends AdminController {

	public function getIndex()
	{ 
		$order_years = Order::where('years',date('Y'))->get();
		$order_months = Order::where('months',date('Y-m'))->get();
		$order_days = Order::where('days',date('Y-m-d'))->get();
		$order_all = Order::all();
		return View::make('home',compact('order_years','order_months','order_days','order_all'))->with('title','Anasayfa');
	}
	public function getProfil()
	{
		return View::make('profil')->with('title','Profil');
	}
	public function postProfil()
	{
		$validation = Validator::make(Input::all(), User::$user_validate);
		if($validation->fails())
			return Redirect::back()->with(['message'=>'true', 'title'=>'Uyarı!', 'text'=>'Kullanıcı adınızı boş bıraktınız.', 'type'=>'warning']);
		$user = User::findOrFail(Auth::user()->id);
		$user->username = Input::get('username');
		$user->email = Input::get('email');
		$user->name = Input::get('name');
		if(Input::has('recovery'))
		{
			$user->recovery = Input::get('recovery');
			$user->save();
		}
		if(Input::has('password'))
		{
			$user->password = Hash::make(Input::get('password'));
			$user->save();
		}
		$user->save();
		if($user->save())	
			return Redirect::back()->with(['message'=>'true', 'title'=>'Tebrikler!', 'text'=>'Hesap bilgileriniz başarıyla güncelleştirildi.', 'type'=>'success']);
		else 
			return Redirect::back()->with(['message'=>'true', 'title'=>'Hata!', 'text'=>'Hesap bilgileriniz güncelleştirilemedi!', 'type'=>'error']);
	
	}
}
