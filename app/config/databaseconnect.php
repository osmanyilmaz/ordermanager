<?php

/*
 * Elixir Otomasyon
 * Osman YILMAZ
 * www.astald.com
 * https://github.com/astald/elixir-ordermanager
 */

return array(
	'host'      => 'localhost',
	'database'  => 'laravel4_birtat',
	'username'  => 'root',
	'password'  => '',
); 
