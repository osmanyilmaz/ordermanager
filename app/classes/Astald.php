<?php

/*
 * Elixir Otomasyon 
 * 
 * Osman YILMAZ
 * osmnylmz@outlook.com
 * www.astald.com
 * 12.07.2015
 * 
 * https://github.com/astald/elixir-ordermanager
 */
class Astald {

	/* 
	 * Product Helpers Classes
	 */
	public static function ProductInfo()
	{
		$param = func_get_args();
		if(@$param[0]<>null)
		{
			$product = Product::find($param[0]);
			if(@$param[1]<>null)
				return $product->$param[1];
			else 
				return $product->id;
		}
		else 
			return 0;
	}
	public static function ProductPrices()
	{
		$param = func_get_args();
		if(@$param[0]<>null)
		{
			if($param[0]=='todays')
				$orders = Order::where('days',date('Y-m-d'))->get();
			else if($param[0]=='months')
				$orders = Order::where('months',date('Y-m'))->get();
			else if($param[0]=='years') 
				$orders = Order::where('years',date('Y'))->get();
			else 
				$orders = Order::all();
			if(count($orders)>0)
			{
				$priceall = 0;
				foreach($orders as $order)
				{
					$otplist = OrderToProduct::where('order_id',$order->id)->get();
					foreach ($otplist as $value)
						$priceall += $value->number*self::ProductInfo($value->product_id,'price');
				}
				return $priceall;
			}
			else 
				return 0;
		}
		else 
			return 0;
	}
	/*
	 * Password recovery 
	 */
	public static function Password()
	{
		$param = func_get_args();
		return sha1(md5(sha1($param[0])));
	}
}
