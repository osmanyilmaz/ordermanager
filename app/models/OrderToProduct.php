<?php

/*
 * Elixir Otomasyon
 * Osman YILMAZ
 * www.astald.com
 * https://github.com/astald/elixir-ordermanager
 */

class OrderToProduct extends Eloquent {
	
	protected $table = "db_order_products";

	public static $validation = ['item_val'=>'required'];

	public $timestamps  = false;

}

