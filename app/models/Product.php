<?php

/*
 * Elixir Otomasyon
 * Osman YILMAZ
 * www.astald.com
 * https://github.com/astald/elixir-ordermanager
 */

class Product extends Eloquent {

	protected $table = "db_products";
	public static $validation = array('title'=>'required','price'=>'required');

}
