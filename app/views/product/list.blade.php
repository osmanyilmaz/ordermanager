@extends('layout.master')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
	<a href="{{url('product/add')}}" class="btn btn-info btn-md pull-right"><i class="fa fa-plus"></i> Yeni ürün ekle</a>
		<h3>Ürünler</h3>
	</div>
	@if($list)
	<table class="table table-hover ">
		<thead>
			<tr>
				<th>Ürün Adı</th>
				<th>Ürün Fiyatı</th>
				<th>Ürün Tipi</th>
				<th colspan="2" style="width:12%">İşlemler</th>
			</tr>
		</thead>
		<tbody>
			@foreach($list as $item)
			<tr>
				<td>{{$item->title}}</td>
				<td>{{$item->price}} TL</td>
				<td>{{ ($item->type == 2 ? 'Sıvı Gıda' : 'Katı Gıda') }}</td>
				<td>
				<a href="{{url('product/edit/'.$item->id)}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Düzelt</a>
				<a href="{{url('product/delete/'.$item->id)}}" class="btn btn-info btn-xs" onclick="return confirm('Ürün kaydını silmek istediğinizden emin misiniz?');"><i class="fa fa-trash"></i> Sil</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<div class="panel-body">
		<div class="alert alert-info"><b>Bigilendirme!</b> Sistemde ürün kaydı bulunamadı!</div>
	</div>
	@endif
</div>

@stop