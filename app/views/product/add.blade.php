@extends('layout.master')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
	<a href="{{url('product')}}" class="btn btn-info btn-md pull-right"><i class="fa fa-chevron-left"></i> Ürünler</a>
		<h3>Ürün Ekle</h3>
	</div>
	<div class="panel-body">
		{{Form::open(['url'=>'product/add','method'=>'post'])}}
			@include('product.form')
		{{Form::close()}}
	</div>
</div>
@stop