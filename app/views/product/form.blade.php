	<div class="row">
		<div class="col-md-4 col-xs-12">
			<div class="form-group"> 
				{{ Form::label('type','Ürün Türü') }}
				{{ Form::select('type',[1=>'Yiyecek - Katı Gıda',2=>'İçecek - Sıvı Gıda'], null, ['class'=>'form-control']) }}
			</div>
		</div>
		<div class="col-md-4 col-xs-12">
			<div class="form-group"> 
				{{ Form::label('title','Ürün Başlık') }}
				{{ Form::text('title', null, array('placeholder'=>'Başlık','class'=>'form-control','autocomplete'=>'off','required'=>true)) }} 
			</div>
		</div>
		<div class="col-md-4 col-xs-12">
			<div class="form-group"> 
				{{ Form::label('price','Ürün Fiyat') }}
				<div class="input-group">
					{{ Form::text('price', null, array('placeholder'=>'Fiyat','class'=>'form-control','autocomplete'=>'off','required'=>true)) }} 
					<span class="input-group-addon">TL</span>
				</div>
			</div>
		</div>
	</div>
	<a href="javascript:void()" onClick="history.go(-1)" class="btn btn-info btn-sm hidden-sm hidden-xs"> <i class="glyphicon glyphicon-chevron-left"></i> Geri dön</a>
	<a href="{{url('product')}}" class="btn btn-danger btn-sm "><i class="glyphicon glyphicon-remove"></i> İptal Et</a>
	{{ Form::button('<i class="glyphicon glyphicon-ok"></i> Ürünü Kaydet', array('type'=>'submit','class'=>'btn btn-primary  btn-md pull-right','data-loading-text'=>'Kaydediliyor...')) }}