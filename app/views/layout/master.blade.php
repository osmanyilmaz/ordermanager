<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Elixir Kebap - Otomasyon</title>
	<link rel="stylesheet" href="{{url('static/plugin/open-sans/open-sans.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/font-awesome/v4.3.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/bootstrap/v3.3.5/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/bootstrap/theme/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/select2-master/dist/css/select2.min.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/select2-master/dist/css/select2-bootstrap.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/pnotify-master/pnotify.custom.min.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/x-editable/1.5.1/css/bootstrap-editable.css')}}">
	<link rel="stylesheet" href="{{url('static/css/main.css')}}">
	@yield('style')
</head>
<body>
<div class="container">
<!-- .header -->
<div class="header">
	<nav class="navbar navbar-default">
	  <div class="container-fluid"> 
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-mobil" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">Elixir Otomasyon</a>
	    </div> 
	    <div class="collapse navbar-collapse" id="navbar-collapse-mobil">
	      <ul class="nav navbar-nav">
	        <li{{ (Request::segment(1)=='' or Request::segment(1)=='home') ? ' class="active"' : null }}><a href="{{url('home')}}"> <i class="fa fa-home"></i> Anasayfa <span class="sr-only">(current)</span></a></li>
	        <li{{ (Request::segment(1)=='order') ? ' class="active"' : null }}><a href="{{url('order')}}"><i class="fa fa-list-ul"></i> Siparişler</a></li>
	        <li{{ (Request::segment(1)=='product') ? ' class="active"' : null }}><a href="{{url('product')}}"><i class="fa fa-cutlery"></i> Ürünler</a></li>
	        <!-- <li{{ (Request::segment(1)=='customer') ? ' class="active"' : null }} class="disabled"><a href="#{{url('customer')}}" disabled="disabled"><i class="fa fa-users"></i> Müşteriler</a></li> -->
	      </ul>
	      <ul class="nav navbar-nav navbar-right"> 
	      	<li class="dropdown {{ (Request::segment(2)=='profil') ? 'active' : null }}">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> {{Auth::user()->name}} <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="{{url('home/profil')}}"><i class="fa fa-user-secret"></i> Hesap bilgileri</a></li>
	            <li><a href="{{url('logout')}}" onclick="return confirm('Çıkış yapmak istediğinizden emin misiniz?');"><i class="fa fa-sign-out"></i> Çıkış yap</a></li> 
	          </ul>
	      </ul>
	      <form action="{{url('order/search')}}" class="navbar-form navbar-right" role="search">
	        <div class="form-group">
	          <input type="text" class="form-control input-sm" name="s" placeholder="Sipariş Ara">
	        </div>
	        <button type="submit" class="btn btn-info btn-sm">Arama yap</button>
	      </form> 
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

</div>
<!-- .header -->
<!-- content -->
<div class="content">
@yield('content')
</div>
<!-- content -->
<!-- footer -->
<div class="footer"> <hr> 
	&copy; Copyright {{date('Y')}} - <a href="https://github.com/osmanyilmazco/elixir-automation">Elixir Otomasyon Sistemi</a>. Tüm hakları mahfuzdur. 
	<span class="pull-right"> &copy; Web Developer <a href="http://www.astald.com" target="_blank">Astald</a> </span>  
</div>
<!-- footer -->
</div>
<!-- js -->
<script src="{{url('static/js/jquery.min.js')}}"></script> 
<script src="{{url('static/plugin/bootstrap/v3.3.5/js/bootstrap.min.js')}}"></script>
<script src="{{url('static/plugin/select2-master/dist/js/select2.min.js')}}"></script>
<script src="{{url('static/plugin/select2-master/dist/js/i18n/tr.js')}}"></script>
<script src="{{url('static/plugin/pnotify-master/pnotify.custom.min.js')}}"></script>
<script src="{{url('static/plugin/jquery.maskedinput/1.4.0/js/jquery.maskedinput.min.js')}}"></script>
<script src="{{url('static/plugin/x-editable/1.5.1/js/bootstrap-editable.min.js')}}"></script>
@if(Session::has('message'))
<script type="text/javascript">(new PNotify({title: "{{Session::get('title')}}",text: "{{Session::get('text')}}",type: "{{Session::get('type')}}"}))</script>
@endif
<script type="text/javascript">
$(function(){
if($.fn.select2) { $('.select2').select2({language:"tr",dir: "ltr"}); }
if ($.fn.mask) {
	$("input.maskPhone").mask("(999) 999 99 99", {placeholder:"_"});
	$('[data-mask]').each(function(i){ 
		$(this).mask($(this).data('mask'), {placeholder:"_"});
	});
}
$('.popoveractive').popover({animation:true,html:true});
});
</script>
@yield('script')
</body>
</html> 