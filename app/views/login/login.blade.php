<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Elixir Otomasyon - Giriş</title>
	<link rel="stylesheet" href="{{url('static/plugin/open-sans/open-sans.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/font-awesome/v4.3.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/bootstrap/v3.3.5/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/bootstrap/theme/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{url('static/css/main.css')}}"> 
	<script src="{{url('static/js/jquery.min.js')}}"></script>
	<script src="{{url('static/plugin/bootstrap/v3.3.5/js/bootstrap.min.js')}}"></script>
</head>
<body>
<div class="container login">
	<div class="login-box">  
		<h2 class="page-header">Yetkili Giriş</h2>
		{{Form::open(['url'=>'logincontroll','method'=>'post','role'=>'form'])}}
			@if(Session::has('messageControl'))
			<div class="alert alert-{{ Session::get('type') }}"> <b>{{ Session::get('title') }}</b> {{ Session::get('text') }}</div>
			@endif  
		    <div class="form-group">
		    	<label for="username">Kullanıcı Adı:</label>
		        <input type="text" class="form-control input-md" name="username" placeholder="Kullanıcı adı" id="username">
		    </div>
		    <div class="form-group">
		    	<label for="password">Şifre:</label>
		        <input type="password" class="form-control input-md" name="password" placeholder="Şifre" id="password">
		    </div> 
		    <div class="form-group">
		    	<label>
			      <input type="checkbox" name="remember"> Beni hatırla
			    </label>
		    </div>
		    <div class="form-group">
		        <button  class="btn btn-primary btn-md btn-block">Giriş yap</button> 
		    </div> 
		    <hr>
		    <a href="{{url('password')}}" class="btn btn-link btn-sm pull-right"><i class="fa fa-circle"></i> Şifre mi unuttum?</a>
		{{Form::close()}}
	</div>
</div>
</body>
</html>