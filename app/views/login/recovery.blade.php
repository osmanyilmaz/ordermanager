<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Elixir Otomasyon - Şifre Kurtarma</title>
	<link rel="stylesheet" href="{{url('static/plugin/open-sans/open-sans.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/font-awesome/v4.3.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/bootstrap/v3.3.5/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{url('static/plugin/bootstrap/theme/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{url('static/css/main.css')}}"> 
	<script src="{{url('static/js/jquery.min.js')}}"></script>
	<script src="{{url('static/plugin/bootstrap/v3.3.5/js/bootstrap.min.js')}}"></script>
</head>
<body>
<div class="container login">
	<div class="login-box"> 
		<h2 class="page-header">Şifre değiştirme</h2>
		{{Form::open(['url'=>'recoverycontroll','method'=>'post','role'=>'form'])}}{{Form::hidden('s',$i)}}
			@if(Session::has('messageControl'))
			<div class="alert alert-{{ Session::get('type') }}"> <b>{{ Session::get('title') }}</b> {{ Session::get('text') }}</div>
			@endif 
		    <div class="form-group">
		    	<label for="newpassword">Yeni şifre:</label>
		        <input type="password" class="form-control input-md" name="newpassword" placeholder="Şifrenizi giriniz" id="newpassword">
		    </div> 
		    <div class="form-group">
		        <button  class="btn btn-primary btn-md btn-block"><i class="fa fa-refresh"></i> Şifreyi değiştir</button> 
		    </div> 
		    <hr>
		    <a href="{{url('login')}}" class="btn btn-link btn-sm pull-right"><i class="fa fa-sign-out"></i> İptal et - giriş yap</a>
		{{Form::close()}}
	</div>
</div>
</body>
</html> 