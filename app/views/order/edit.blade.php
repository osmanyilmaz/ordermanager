@extends('layout.master')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
	<a href="{{url('order')}}" class="btn btn-info btn-md pull-right"><i class="fa fa-chevron-left"></i> Sipariş Listesi</a>
		<h3>Sipariş Düzelt</h3>
	</div>
	<div class="panel-body">
		{{Form::model($edit,['url'=>'order/edit/'.$edit->id,'method'=>'post'])}}
			<!-- add-form -->	
			<div class="row">
				<div class="col-md-7 col-xs-12">	
				@if(count($list_product)>0)
					<table class="table table-hover ">
						<thead>
							<tr>
								<th>Ürün Adı</th>
								<th>Ürün Fiyatı</th>
								<th>Sipariş Adeti</th>
							</tr>
						</thead>
						<tbody>
						{{--*/ $dni = 0; /*--}}
							@foreach($list_product as $item)
							@if($item->number>0)
							<tr id="sln_{{$dni}}" class="sln_{{$dni}}">
								<td>{{Astald::ProductInfo($item->product_id,'title')}}</td>
								<td>{{Astald::ProductInfo($item->product_id,'price')}} TL</td>
								<td style="width:20%"><b><a href="#" class="selectNumber text-bold" data-type="select" data-pk="1" data-value="{{$item->number}}" data-productid="{{$item->product_id}}" data-number="{{$dni}}" data-orderid="{{$item->order_id}}" data-selectnumber="{{$item->number}}" data-price="{{Astald::ProductInfo($item->product_id,'price')}}" data-title="Sipariş Adeti"></a></b></td>
							</tr>
							@else
							<tr id="sln_{{$dni}}" class="danger sln_{{$dni}}">
								<td>{{Astald::ProductInfo($item->product_id,'title')}}</td>
								<td>{{Astald::ProductInfo($item->product_id,'price')}} TL</td>
								<td style="width:20%"><b><a href="#" class="selectNumber text-bold" data-type="select" data-pk="1" data-value="{{$item->number}}" data-productid="{{$item->product_id}}" data-number="{{$dni}}" data-orderid="{{$item->order_id}}" data-selectnumber="{{$item->number}}" data-price="{{Astald::ProductInfo($item->product_id,'price')}}" data-title="Sipariş Adeti"></a></b></td>
							</tr>
							@endif
							{{--*/ $dni++ /*--}}
							@endforeach
						</tbody> 
					</table> 
				@else
				<div class="alert alert-info"><b>Bilgilendirme!</b> Sipariş için kayıtlı ürün bulunamadı.</div>
				@endif

				</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group"> 
						{{ Form::label('name','Sipariş Ad Soyad') }}
						{{ Form::text('name', null, array('placeholder'=>'Başlık','class'=>'form-control')) }} 
					</div>
					<div class="form-group"> 
						{{ Form::label('phone','Sipariş Telefonu') }}
						{{ Form::text('phone', null, array('placeholder'=>'Telefon','class'=>'form-control maskPhone','type'=>'tel','data-mask'=>'(999) 999 99 99','id'=>'phone')) }} 
					</div>
					<div class="form-group"> 
						{{ Form::label('address','Sipariş Adress') }}
						{{ Form::textarea('address', null, ['placeholder'=>'Adres','class'=>'form-control','rows'=>'3']) }}
					</div>
					<hr>
					{{ Form::button('<i class="glyphicon glyphicon-ok"></i> Siparişi Kaydet', array('type'=>'submit','class'=>'btn btn-primary  btn-md pull-right','data-loading-text'=>'Kaydediliyor...')) }}

				</div>
			</div>  
			<hr>

			<a href="javascript:void()" onClick="history.go(-1)" class="btn btn-info btn-sm hidden-sm hidden-xs"> <i class="glyphicon glyphicon-chevron-left"></i> Geri dön</a>
			<a href="{{url('order')}}" class="btn btn-danger btn-sm "><i class="glyphicon glyphicon-remove"></i> İptal Et</a>
			
		{{Form::close()}}
	</div>
</div> 
@stop
@section('style')
@stop
@section('script')
<script type="text/javascript">
$(document).ready(function(){ 
	var selects = $('.selectNumber');
	selects.editable({ source: [ @for($i = 0; $i<=100; $i++) {value: {{$i}}, text: {{$i}}}, @endfor ] }); 
    selects.on('save', function(e, params) {  
		var number = $(this).attr('data-number'), valuess = params.newValue,productid = $(this).attr('data-productid'), orderid= $(this).attr('data-orderid');
		$.post('{{url("order/edittable")}}', {_token:"{{csrf_token()}}",productid:productid,orderid:orderid,number:valuess});
		if(valuess>0)  $(".sln_"+number).removeClass('danger'); 
		else   $(".sln_"+number).addClass('danger'); 
    }); 
	// onlineNumber hidden value change
	// $('.select2').change(function(e){ e.preventDefault; var id = $(this).attr('data-id'), value = $(this).val(); $('input[data-id='+id+']').val(value); });
});
</script>
@stop