	
	<div class="row">
		<div class="col-md-7 col-xs-12">	
		@if($list_product)
			<table class="table table-hover ">
				<thead>
					<tr>
						<th>Ürün Adı</th>
						<th>Ürün Fiyatı</th>
						<th>Sipariş Adeti</th>
					</tr>
				</thead>
				<tbody>
				{{--*/ $dni = 0; /*--}}
					@foreach($list_product as $item)
					<tr>
						<td>{{$item->title}}</td>
						<td>{{$item->price}} TL</td>
						<td style="width:20%">
						{{Form::selectRange('number', 0, 100, 0, ['placeholder'=>'Ürün Adeti','class'=>'form-control input-sm select2','data-id'=>$item->id,'data-number'=>$dni,'data-price'=>$item->price])}}
						{{Form::hidden('item_val['.$item->id.']',0,['class'=>'onlineNumber','data-id'=>$item->id])}}   
						</td>
					</tr>
					{{--*/ $dni++ /*--}}
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3"><div class="text-right">Siparişiniz için hazır olan toplam ürün: <b>{{count($list_product)}}</b></div></td>
					</tr>
				</tfoot>
			</table> 
		@else
		<div class="alert alert-warning"><b>Uyarı!</b> Sipariş için eklenecek ürün kaydı bulunamadı! Lütfen ürün ekleyiniz.</div>
		@endif

		</div>
		<div class="col-md-5 col-xs-12">
			<div class="form-group"> 
				{{ Form::label('name','Sipariş Ad Soyad') }}
				{{ Form::text('name', null, array('placeholder'=>'Başlık','class'=>'form-control')) }} 
			</div>
			<div class="form-group"> 
				{{ Form::label('phone','Sipariş Telefonu') }}
				{{ Form::text('phone', null, array('placeholder'=>'Telefon','class'=>'form-control')) }} 
			</div>
			<div class="form-group"> 
				{{ Form::label('address','Sipariş Adress') }}
				{{ Form::textarea('address', null, ['placeholder'=>'Adres','class'=>'form-control','rows'=>'3']) }}
			</div>
		<h1 id="priceId" class="text-center well">
			<b>Toplam Fiyat: <span class="priceField">0</span> TL</b>
		</h1>
		<a href="javascript:void(0);" onClick="window.location.refresh();" class="btn btn-info btn-sm hidden-sm hidden-xs"> <i class="glyphicon glyphicon-refresh"></i> Sayfayı Yenile</a>
		</div>
	</div>  
	<hr>

	<a href="javascript:void()" onClick="history.go(-1)" class="btn btn-info btn-sm hidden-sm hidden-xs"> <i class="glyphicon glyphicon-chevron-left"></i> Geri dön</a>
	<a href="{{url('order')}}" class="btn btn-danger btn-sm "><i class="glyphicon glyphicon-remove"></i> İptal Et</a>
	{{ Form::button('<i class="glyphicon glyphicon-ok"></i> Siparişi Kaydet', array('type'=>'submit','class'=>'btn btn-primary  btn-md pull-right','data-loading-text'=>'Kaydediliyor...')) }}
