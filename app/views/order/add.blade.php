@extends('layout.master')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
	<a href="{{url('order')}}" class="btn btn-info btn-md pull-right"><i class="fa fa-chevron-left"></i> Sipariş Listesi</a>
		<h3>Sipariş Ekle</h3>
	</div>
	<div class="panel-body">
		{{Form::open(['url'=>'order/add','method'=>'post'])}}
			<!-- add-form -->	
			<div class="row">
				<div class="col-md-7 col-xs-12">	
				@if($list_product)
					<table class="table table-hover ">
						<thead>
							<tr>
								<th>Ürün Adı</th>
								<th>Ürün Fiyatı</th>
								<th>Sipariş Adeti</th>
							</tr>
						</thead>
						<tbody>
						{{--*/ $dni = 0; /*--}}
							@foreach($list_product as $item)
							<tr>
								<td>{{$item->title}}</td>
								<td>{{$item->price}} TL</td>
								<td style="width:20%">
								{{Form::selectRange('number', 0, 100, 0, ['placeholder'=>'Ürün Adeti','class'=>'form-control input-sm select2','data-id'=>$item->id,'data-number'=>$dni,'data-price'=>$item->price])}}
								{{Form::hidden('item_val['.$item->id.']',0,['class'=>'onlineNumber','data-id'=>$item->id])}}   
								</td>
							</tr>
							{{--*/ $dni++ /*--}}
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3"><div class="text-right">Siparişiniz için hazır olan toplam ürün: <b>{{count($list_product)}}</b></div></td>
							</tr>
						</tfoot>
					</table> 
				@else
				<div class="alert alert-warning"><b>Uyarı!</b> Sipariş için eklenecek ürün kaydı bulunamadı! Lütfen ürün ekleyiniz.</div>
				@endif
				</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group"> 
						{{ Form::label('name','Sipariş Ad Soyad') }}
						{{ Form::text('name', null, array('placeholder'=>'Başlık','class'=>'form-control')) }} 
					</div>
					<div class="form-group"> 
						{{ Form::label('phone','Sipariş Telefonu') }}
						{{ Form::text('phone', null, array('placeholder'=>'Telefon','class'=>'form-control maskPhone','type'=>'tel','data-mask'=>'(999) 999 99 99')) }} 
					</div>
					<div class="form-group"> 
						{{ Form::label('address','Sipariş Adress') }}
						{{ Form::textarea('address', null, ['placeholder'=>'Adres','class'=>'form-control','rows'=>'3']) }}
					</div>
					<h1 id="priceId" class="text-center well">
						<b>Toplam Fiyat: <span class="priceField">0</span> TL</b>
					</h1> 
					<hr>
					{{ Form::button('<i class="glyphicon glyphicon-ok"></i> Siparişi Kaydet', array('type'=>'submit','class'=>'btn btn-primary btn-md pull-right','data-loading-text'=>'Kaydediliyor...')) }}
				</div>
			</div>  
			<hr>
			<a href="javascript:void()" onClick="history.go(-1)" class="btn btn-info btn-sm hidden-sm hidden-xs"> <i class="glyphicon glyphicon-chevron-left"></i> Geri dön</a>
			<a href="{{url('order')}}" class="btn btn-danger btn-sm "><i class="glyphicon glyphicon-remove"></i> İptal Et</a>
			<a href="javascript:void(0);" onClick="window.location.reload();" class="btn btn-info btn-sm hidden-sm hidden-xs pull-right"> <i class="glyphicon glyphicon-refresh"></i> Sayfayı Yenile</a>
			
			<!-- add-form -->
		{{Form::close()}}
	</div>
</div> 
@stop
@section('style')
@stop
@section('script')
<script type="text/javascript">
$(document).ready(function(){
	<?php $iCount=0; ?>
	var params = [],paramsPrice = []; 
	@foreach($list_product as $item)
	params[{{$iCount}}] = 0; 
	paramsPrice[{{$iCount}}] = "{{$item->price}}"; 
	<?php $iCount++; ?>
	@endforeach 
	
	 
	$('.select2').bind('change',function(e){
		e.preventDefault;
		var priceResult = 0, 
			id = $(this).attr('data-id'), 
			number = $(this).attr('data-number'),
			price = $(this).attr('data-price');
 
		params[number] = $(this).val();
		paramsPrice[number] = price;
 
		$(params).each(function(index, item){ 
			priceResult += item * paramsPrice[index]; 
			//console.log("params["+index+"]: "+item); 
		}); 

		var priceField = $(".priceField").html(priceResult);
 
	});
	 
	// onlineNumber hidden value change
	$('.select2').change(function(e){ e.preventDefault; var id = $(this).attr('data-id'), value = $(this).val(); $('input[data-id='+id+']').val(value); });
});
</script>
@stop