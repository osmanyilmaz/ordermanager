@extends('layout.master')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<h3 class="pull-left">{{$datename}} Siparişler</h3> 
		<a href="{{url('order/add')}}" class="btn btn-info btn-md pull-right"><i class="fa fa-plus"></i> Yeni sipariş ekle</a>
		@if($date_visible)
		<div class="dropdown dropdown-toggle pull-right"> 
		  <button class="btn btn-info btn-md  m-r-10" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		    <i class="fa fa-calendar"></i> Tarihe göre sırala <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		    <li{{($datetime=='today' ? ' class="active"': null)}}><a href="{{url('order/list/today')}}">Bu gün ({{date('Y-m-d')}})</a></li>
		    <li{{($datetime=='month' ? ' class="active" ': null)}}><a href="{{url('order/list/month')}}">Bu ay ({{date('Y-m')}})</a></li> 
		    <li{{($datetime=='year' ? ' class="active" ': null)}}><a href="{{url('order/list/year')}}">Bu yıl ({{date('Y')}})</a></li> 
		    <li{{($datetime=='all' ? ' class="active" ': null)}}><a href="{{url('order/list/all')}}" class="text-center"><i class="fa fa-list"></i> Tümünü Göster</a></li> 
		  </ul>  
		</div>
		@endif
	</div>
	<div class="panel-body">
	@if($count_static)
	<div class="home">
		<div class="row">
			<div class="col-md-3"> <div class="well text-center"><h1>
				<small>Bugunün toplam siparişleri</small> <br>
				{{count($order_days)}} <br>
				<small class="price">Toplam Fiyat: {{Astald::ProductPrices('todays')}} TL</small>
				</h1></div> </div>
			<div class="col-md-3"> <div class="well text-center"><h1>
				<small>Bu ayın toplam siparişleri</small> <br>
				{{count($order_months)}}<br>
				<small class="price">Toplam Fiyat: {{Astald::ProductPrices('months')}} TL</small>
			</h1></div> </div>
			<div class="col-md-3"> <div class="well text-center"><h1>
				<small>Bu yılın toplam siparişleri</small> <br>
				{{count($order_years)}}<br>

				<small class="price">Toplam Fiyat: {{Astald::ProductPrices('years')}} TL</small>
			</h1></div> </div>
			<div class="col-md-3"> <div class="well text-center"><h1>
				<small>Toplam siparişler</small>
				<br> {{count($order_all)}}<br>
				<small class="price">Toplam Fiyat: {{Astald::ProductPrices('all')}} TL</small>
			</h1></div> </div>
		</div>
	</div>
	<hr>
	@endif
	@if(count($list)>0) 
	<div class="row">
		@foreach($list as $item) 
		<div class="col-md-3 col-xs-12">
		  <div class="panel {{$item->days == date('Y-m-d') ? 'panel-danger' : 'panel-default'}} product-lists ">
		  <div class="panel-heading"><span class="">Sipariş no: #{{$item->id}}</span>
		  <div class="dropdown pull-right">
		  	<a href="#" class="btn  {{$item->days == date('Y-m-d') ? 'btn-danger' : 'btn-info'}} btn-sm dropdown-toggles " data-toggle="dropdown" role="button" aria-expanded="false">İşlemler <span class="caret"></span></a>
	           <ul class="dropdown-menu" role="menu">
	            <li><a href="{{url('order/edit/'.$item->id)}}"><i class="fa fa-pencil"></i> Sipariş düzelt</a></li>
	            <li><a href="{{url('order/delete/'.$item->id)}}" onclick="return confirm('Sipariş kaydını silmek üzeresiniz ? Kaydı silmek istediğinizden emin misiniz?');"><i class="fa fa-trash"></i> Sipariş sil</a></li> 
	           </ul>
		  </div> 
		  </div>
			<ul class="list-group">
				<li class="list-group-item"><b>Telefon:</b> {{($item->phone<>"" ? $item->phone : '<span class="label label-danger">Numara yok</span>')}}</li>
				<li class="list-group-item"><b>Ad Soyad:</b> {{($item->name<>"" ? $item->name : '<span class="label label-danger">Ad soyad yok</span>')}}</li>
				<li class="list-group-item"><b>Adres:</b> <button type="button" class="btn btn-xs btn-info popoveractive" data-toggle="popover" data-placement="top"  title="Adres" data-content="{{($item->address<>"" ? $item->address : 'Adres kaydı bulunamadı!')}}"><i class="fa fa-map-marker"></i> Adresi görüntüle</button></li>
				@if(count(OrderToProduct::where('order_id',$item->id)->get())>0)
				<li class="list-group-item product-list"> 
					<table class="table table-hover">
					<thead>
						<tr>
							<th>Ürün</th>
							<th>Adet</th>
							<th>Top.Fiyat</th>
						</tr>
					</thead>
					<tbody>
					@foreach(OrderToProduct::where('order_id',$item->id)->get() as $itemp)
					@if($itemp->number>0)
					{{--*/ $price_all += $itemp->number*Astald::ProductInfo($itemp->product_id,'price') /*--}}
						<tr>
							<td>{{Astald::ProductInfo($itemp->product_id,'title')}}</td>
							<td>{{$itemp->number}}</td>
							<td>{{$itemp->number*Astald::ProductInfo($itemp->product_id,'price')}} TL</td>
						</tr> 
					@endif
					@endforeach
					</tbody>
					</table>
				</li>
				@else
				<li class="product-list"> <div class="panel-body text-center"> <br> <br> <br> <div class="alert alert-info"><b>Bilgilendirme</b><br> Ürün kaydı bulunamadı!</div> </div> </li>
				@endif
				<li class="list-group-item text-center">
					@if(count(OrderToProduct::where('order_id',$item->id)->get())>0)
					<h4>Toplam Fiyat: {{$price_all}} TL</h4>
					 {{--*/ $price_all=0; /*--}}
					@else
					<h4><span class="label label-danger">Toplam fiyat bulunamadı!</span></h4>
					@endif
				</li>
			</ul>
		  </div>
		</div>
		@endforeach
	</div>
	{{$list->links()}}
	@else
		<div class="alert alert-info m-b-0"><b>Bigilendirme!</b> Sistemde sipariş kaydı bulunamadı!</div>
	@endif
	</div>
</div>

@stop