@extends('layout.master')
@section('content')
	<div class="panel panel-default">
	<div class="panel-heading"><h3>Hesap Bilgileriniz</h3></div>
	<div class="panel-body">
		{{Form::open(['url'=>url('home/profil'),'method'=>'post'])}}
		<div class="row">
			<div class="col-md-6 col-xs-12">			
			<div class="form-group">
				{{Form::label('username','Kullanıcı adı')}}
				{{Form::text('username', Auth::user()->username, ['class'=>'form-control'])}}
			</div>
			</div>
			<div class="col-md-6 col-xs-12">			
			<div class="form-group">
				{{Form::label('password','Şifre')}} 
				{{Form::password('password', array('class'=>'form-control','placeholder'=>'Şifre'))}} 
			</div>
			</div>
			<div class="col-md-6 col-xs-12">			
			<div class="form-group">
				{{Form::label('name','Ad soyad')}}
				{{Form::text('name', Auth::user()->name, ['class'=>'form-control'])}}
			</div>
			</div>
			<div class="col-md-6 col-xs-12">			
			<div class="form-group">
				{{Form::label('email','Email adresi')}}
				{{Form::text('email', Auth::user()->email, ['class'=>'form-control'])}}
			</div>
			</div>
		</div>
			<div class="form-group has-success">
				{{Form::label('recovery',"Güvenlik kodu:")}} <i class="fa fa-arrow-right"></i> Geçerli kodunuz: <b> {{Auth::user()->recovery}}</b>
				{{Form::text('recovery', Auth::user()->recovery, ['class'=>'form-control','placeholder'=>'Kodu değiştirmek istediğiniz zaman giriniz'])}} 
			</div>
		<hr>
		<a href="javascript:void()" onClick="history.go(-1)" class="btn btn-info btn-sm hidden-sm hidden-xs"> <i class="glyphicon glyphicon-chevron-left"></i> Geri dön</a>
		{{ Form::button('<i class="glyphicon glyphicon-ok"></i> Bilgileri değiştir', array('type'=>'submit','class'=>'btn btn-primary  btn-md pull-right','data-loading-text'=>'Kaydediliyor...')) }}
		{{Form::close()}}
	</div>
	</div>
@stop