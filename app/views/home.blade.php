@extends('layout.master')
@section('content')
<div class="well well-bg text-center">
	<h1>Elixir Kebap Otomasyon Sistemi <small><small>v1.0</small></small></h1>
</div>
<div class="home">
	<div class="row">
		<div class="col-md-3"> <div class="well text-center"><h1>
			<small>Bugunün toplam siparişleri</small> <br>
			{{count($order_days)}} <br>
			<small class="price">Toplam Fiyat: {{Astald::ProductPrices('todays')}} TL</small>
			</h1></div> </div>
		<div class="col-md-3"> <div class="well text-center"><h1>
			<small>Bu ayın toplam siparişleri</small> <br>
			{{count($order_months)}}<br>
			<small class="price">Toplam Fiyat: {{Astald::ProductPrices('months')}} TL</small>
		</h1></div> </div>
		<div class="col-md-3"> <div class="well text-center"><h1>
			<small>Bu yılın toplam siparişleri</small> <br>
			{{count($order_years)}}<br>

			<small class="price">Toplam Fiyat: {{Astald::ProductPrices('years')}} TL</small>
		</h1></div> </div>
		<div class="col-md-3"> <div class="well text-center"><h1>
			<small>Toplam siparişler</small>
			<br> {{count($order_all)}}<br>
			<small class="price">Toplam Fiyat: {{Astald::ProductPrices('all')}} TL</small>
		</h1></div> </div>
	</div>
</div>
@stop