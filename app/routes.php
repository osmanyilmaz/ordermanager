<?php

/*
|--------------------------------------------------------------------------
| Application Routes
| For ordering system private
|--------------------------------------------------------------------------
|
| Elixir Otomasyon
| Osman YILMAZ
| www.astald.com
| 13.07.2015
|
| https://github.com/osmanyilmazco/elixir-automation
|
*/

View::share(['maintitle'=>"Birtat Kebap"]);

// auth -> after : for login
Route::group(array('after' => 'auth'), function(){
	Route::get('login', 'Astald_LoginController@login'); 
	Route::post('logincontroll', 'Astald_LoginController@logincontroll'); 
	Route::get('password', 'Astald_LoginController@password'); 
	Route::post('passwordcontroll', 'Astald_LoginController@passwordcontroll'); 
	Route::get('recovery/{i}/{s}/i{d}', 'Astald_LoginController@recovery')->where(['i'=>'[A-Za-z-_0-9]+','s'=>'[A-Za-z-_0-9]+','d'=>'[0-9]+']); 
	Route::post('recoverycontroll', 'Astald_LoginController@recoverycontroll'); 
}); 
// auth -> before : for controll
Route::group(array('before' => 'auth'), function(){
	Route::get('/', 'Astald_HomeController@getIndex');
	Route::controller('home', 'Astald_HomeController'); 
	Route::controller('product', 'Astald_ProductsController');  
	Route::controller('order', 'Astald_OrderController');  
	Route::controller('user', 'Astald_UserController');  
	Route::get('logout', 'Astald_LoginController@logout'); 
});